import adafruit_bno055 as bno055
from adafruit_bmp280 import Adafruit_BMP280_I2C as bmp280
from adafruit_servokit import ServoKit
from time import monotonic, sleep
from enum import IntEnum
from math import log, fabs
import consts
from numpy import array, ndarray, asarray, matmul

# Different modes of operation during flight
class Runmode(IntEnum):
    STANDBY = 0
    LAUNCH = 1
    COAST = 2
    DESCENT = 3


# For checking if everything is okay to go, or not. If there was a problem connecting to
# anything, the program will change the flight status to no go and close everything after
# running through all the initialization procedures, this way we can see what actually did
# connect succesfully instead of cutting the program short whenever there's an error
class FlightStatus(IntEnum):
    GO = 0
    NOGO = 1


def velocity(bmp: bmp280, acceleration: ndarray, dt):
    consts.VELOCITY += acceleration * dt
    consts.VELOCITY[2] = vertical_velocity(bmp, dt)
    return consts.VELOCITY


def position(velocity, dt):
    consts.POSITION += velocity * dt
    return consts.POSITION


def vertical_velocity(bmp: bmp280, dt):
    veloc = (bmp.altitude - consts.ALTITUDE) / dt
    consts.ALTITUDE = bmp.altitude
    return veloc


def vertical_acceleration(bno: bno055.BNO055):
    inert_accel = inertial_acceleration(bno)
    return inert_accel[2]


def projected_altitude(accel, veloc, alt):
    """ The projected apogee of the vehicle. """
    try:
        return (
            fabs((veloc ** 2) / (2 * (accel + consts.GRAV)))
            * log(fabs(accel / consts.GRAV))
            + alt
        )
    except (ValueError, ZeroDivisionError):
        return 0


def sqr(x):
    """ Helper function for squaring values. """
    return x * x


def move_servos(servoKit: ServoKit, degrees):
    """ Move servos to specified degrees. """
    for i in range(len(servoKit.servo)):
        servoKit.servo[i].angle = degrees


def verify_launch(bmp: bmp280, threshold_alt):
    """ Make sure vehicle has actually launched. """
    has_launched = True
    current_time = monotonic()
    while fabs(monotonic() - current_time) < 0.5:
        alt = altitude(bmp)
        if alt < threshold_alt:
            has_launched = False
    return has_launched


def verify_burnout(bno: bno055.BNO055):
    has_burntout = True
    c_time = monotonic()
    while fabs(monotonic() - c_time) < 0.5:
        accel = inertial_acceleration(bno)
        if accel[2] > 0:
            has_burntout = True
            break
    return has_burntout


def verify_apogee(bmp: bmp280, dt):
    is_descending = True
    c_time = monotonic()
    while fabs(monotonic() - c_time) < 0.5:
        velocity = vertical_velocity(bmp, dt)
        if velocity > 0:
            is_descending = False
            break
    return is_descending


def init_current_altitude(bmp: bmp280):
    """ Get an average of the current altitude on the launchpad. """
    current_time = monotonic()
    data = list()
    while fabs(monotonic() - current_time) < 3:
        data.append(bmp.altitude * consts.METERSTOFEET)
    return sum(data) / len(data)


def altitude(bmp: bmp280):
    """ Return altitude in feet instead of meters. """
    return bmp.altitude * consts.METERSTOFEET


def inertial_acceleration(bno: bno055.BNO055) -> ndarray:
    """ Return the inertial of the vehicle, in feet per second. """
    accel = asarray(bno.acceleration).T  # .T to convert row vector to column vector
    t_matrix = vehicle_to_inertial(bno.quaternion)
    inertial_accel = matmul(t_matrix, accel)
    return inertial_accel * consts.METERSTOFEET


def vehicle_to_inertial(quaternion: tuple):
    """ Create a transformation matrix. 
        a, b, c, d are given from quaternions supplied by the bno055. """
    a = quaternion[0]
    b = quaternion[1]
    c = quaternion[2]
    d = quaternion[3]
    return array(
        [
            [
                sqr(a) + sqr(b) - sqr(c) - sqr(d),
                2 * b * c - 2 * a * d,
                2 * b * d + 2 * a * c,
            ],
            [
                2 * b * c + 2 * a * d,
                sqr(a) - sqr(b) + sqr(c) - sqr(d),
                2 * c * d - 2 * a * b,
            ],
            [
                2 * b * d - 2 * a * c,
                2 * c * d + 2 * a * b,
                sqr(a) - sqr(b) - sqr(c) + sqr(d),
            ],
        ]
    )
