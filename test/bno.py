from adafruit_bno055 import BNO055
import sys
import board
import busio
from numpy import array, ndarray, asarray, matmul, set_printoptions
from time import sleep

GRAV = 32.174
METERSTOFEET = 3.2808399

VELOCITY = ndarray(shape=(1, 3))
POSITION: ndarray = ndarray(shape=(1, 3))
ALTITUDE = 0


def sqr(x):
    """ Helper function for squaring values. """
    return x * x


def inertial_acceleration(bno: BNO055) -> ndarray:
    """ Return the inertial of the vehicle, in feet per second. """
    accel = asarray(bno.acceleration).T  # .T to convert row vector to column vector
    t_matrix = vehicle_to_inertial(bno.quaternion)
    inertial_accel = matmul(t_matrix, accel)
    return inertial_accel * METERSTOFEET


def vehicle_to_inertial(quaternion: tuple) -> ndarray:
    """ Create a transformation matrix. 
        a, b, c, d are given from quaternions supplied by the bno055. """
    a = quaternion[0]
    b = quaternion[1]
    c = quaternion[2]
    d = quaternion[3]
    return array(
        [
            [
                sqr(a) + sqr(b) - sqr(c) - sqr(d),
                2 * b * c - 2 * a * d,
                2 * b * d + 2 * a * c,
            ],
            [
                2 * b * c + 2 * a * d,
                sqr(a) - sqr(b) + sqr(c) - sqr(d),
                2 * c * d - 2 * a * b,
            ],
            [
                2 * b * d - 2 * a * c,
                2 * c * d + 2 * a * b,
                sqr(a) - sqr(b) - sqr(c) + sqr(d),
            ],
        ],
        dtype=float,
    )


# Create I2C object
try:
    i2c = busio.I2C(board.SCL, board.SDA)
except RuntimeError:
    print("Could not create 12c object")

# Create connection to BNO055 accelerometer
# try:
bno = BNO055(i2c)
# except (RuntimeError, OSError, ValueError):
#     print("Could not create connection to BNO055")


while True:
    accel = inertial_acceleration(bno)
    x = accel[0]
    y = accel[1]
    z = accel[2]
    alt = bmp280.altitude * METERSTOFEET
    print(f"x: {x:10.3f}    y: {y:10.3f}    z: {z:10.3f}", end="\r")
