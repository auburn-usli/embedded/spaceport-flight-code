"""Example that rotates servos on every channel to 180 and then back to 0."""
import time
from adafruit_servokit import ServoKit

# Set channels to the number of servo channels on your kit.
# 8 for FeatherWing, 16 for Shield/HAT/Bonnet.
kit = ServoKit(channels=16)

while True:
    print("POSITION 1")
    for i in range(len(kit.servo)):
        kit.servo[i].angle = 120
    time.sleep(2)
    print("POSITION 2")
    for i in range(len(kit.servo)):
        kit.servo[i].angle = 0
    time.sleep(2)
