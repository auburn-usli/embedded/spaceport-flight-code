import board
import digitalio
import busio
from adafruit_bmp280 import Adafruit_BMP280_I2C
from time import monotonic, sleep

# Create library object using our Bus I2C port
i2c = busio.I2C(board.SCL, board.SDA)
bmp280 = Adafruit_BMP280_I2C(i2c)


# OR create library object using our Bus SPI port
# spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
# bmp_cs = digitalio.DigitalInOut(board.D10)
# bmp280 = adafruit_bmp280.Adafruit_BMP280_SPI(spi, bmp_cs)


class SMAFilter:
    def __init__(self, length=5):
        self.arr_index = 0
        self.array = [0] * length
        self.length = length

    def output(self, input_val):
        self.array[self.arr_index] = input_val
        total_sum = sum(self.array)
        if self.arr_index < self.length - 1:
            self.arr_index += 1
        else:
            self.arr_index = 0
        return total_sum / self.length


# change this to match the location's pressure (hPa) at sea level
bmp280.seaLevelhPa = 1013.25

while True:
    alt = bmp280.altitude * 3.28
    print(f"Altitude: {alt:>8.3f} ", end="\r")
